package com.mars.module.system.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mars.common.request.sys.SysRoleQueryRequest;
import com.mars.common.response.sys.SysRoleListResponse;
import com.mars.framework.mapper.BasePlusMapper;
import com.mars.module.system.entity.SysRole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 角色
 *
 * @author 源码字节-程序员Mars
 */
public interface SysRoleMapper extends BasePlusMapper<SysRole> {

    /**
     * 根据userId查询角色
     *
     * @param id id
     * @return List<SysRole>
     */
    List<SysRole> selectByUserId(Long id);

    /**
     * 分页查询
     *
     * @param page     page
     * @param queryDto queryDto
     * @return IPage<SysRole>
     */
    IPage<SysRole> selectPageList(@Param("page") IPage<Object> page, @Param("queryDto") SysRoleQueryRequest queryDto);

    /**
     * 根据用户ids查询角色列表
     *
     * @param userIds 用户ids
     * @return List<SysRoleListResponse>
     */
    List<SysRoleListResponse> selectUserRoleList(@Param("userIds") List<Long> userIds);
}
