package com.mars.module.admin.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.mars.module.system.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * 消息数量对象 sys_user_message_stats
 *
 * @author mars
 * @date 2023-12-06
 */

@Data
@ApiModel(value = "消息数量对象")
@Builder
@Accessors(chain = true)
@TableName("sys_user_message_stats")
public class SysUserMessageStats {


    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "id")
    private Long id;

    /**
     * 发送方
     */
    @Excel(name = "用户ID")
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    @ApiModelProperty(value = "用户ID")
    private Long userId;

    /**
     * 消息未读数量
     */
    @ApiModelProperty(value = "消息未读数量")
    private Integer unRead;


}
