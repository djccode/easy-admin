package com.mars.common.response.sys;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 功能描述
 *
 * @author Mars.wq [wqexpore@163.com]
 * @version 1.0
 * @date 2023-11-09 16:54:56
 */
@Data
@ApiModel(value = "父级菜单列表")
public class SysParentMenuResponse {

    @ApiModelProperty(value = "菜单id")
    private Long id;

    @ApiModelProperty(value = "菜单名称")
    private String menuName;

}
